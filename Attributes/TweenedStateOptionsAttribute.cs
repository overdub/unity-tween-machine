using System;

namespace TweenMachine.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class TweenedStateOptionsAttribute : Attribute
    {
        public string[] Ids { get; private set; }
        public string IdsSource { get; private set; }
        
        public TweenedStateOptionsAttribute(string[] ids)
        {
            Ids = ids;
        }
        
        public TweenedStateOptionsAttribute(string idsSource)
        {
            IdsSource = idsSource;
        }
    }
}