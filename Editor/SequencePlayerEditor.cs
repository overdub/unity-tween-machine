using System.Collections.Generic;
using System.Linq;
using DataBinding.Lib.Editor;
using TweenMachine.Properties;
using TweenMachine.Utils;
using UnityEditor;
using UnityEngine;

namespace TweenMachine.Editor
{
    [CustomEditor(typeof(SequencePlayer)), CanEditMultipleObjects]
    public class SequencePlayerEditor : UnityEditor.Editor
    {
        private static SequencePlayerEditor activePreviewInstance;

        private EditableList<SequenceTweenProperty> tweenList;

        private List<SequencePlayer> childSequences = new List<SequencePlayer>();

        private static GUIStyle smallLabelStyle;
        private SequencePlayer rootPlayer;

        public override void OnInspectorGUI()
        {
            var startY = EditorGUILayout.GetControlRect(false, 0).y;

            var multiEdit = Selection.objects.Length > 1;

            var sequenceId = serializedObject.FindProperty("sequenceId");
            var controllableByParent = serializedObject.FindProperty("controllableByParent");
            var sequenceStateMachine = serializedObject.FindProperty("sequenceStateMachine");

            DrawParentSequence(sequenceId, controllableByParent, multiEdit);

            EditorGUI.BeginDisabledGroup(sequenceStateMachine.objectReferenceValue != null);
            DrawSequenceId(sequenceId);

            EditorGUILayout.PropertyField(controllableByParent);

            EditorGUILayout.PropertyField(serializedObject.FindProperty("playOnEnable"));

            EditorGUI.EndDisabledGroup();

            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUI.indentLevel++;
            var showSequenceSettings = serializedObject.FindProperty("showSequenceSettings");
            showSequenceSettings.boolValue = EditorGUILayout.Foldout(showSequenceSettings.boolValue, "Sequence settings");
            EditorGUI.indentLevel--;
            if (showSequenceSettings.boolValue)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("delay"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("timeScale"));
                EditorGUILayout.BeginHorizontal();
                var loopProperty = serializedObject.FindProperty("loop");
                EditorGUILayout.PropertyField(loopProperty);
                if (loopProperty.boolValue)
                {
                    var loopCountProperty = serializedObject.FindProperty("loopCount");
                    loopCountProperty.intValue = EditorGUILayout.IntField(loopCountProperty.intValue, GUILayout.Width(50));
                    
                    var yoyoProperty = serializedObject.FindProperty("yoyo");
                    var rect = EditorGUILayout.GetControlRect(GUILayout.Width(50));
                    yoyoProperty.boolValue = GUI.Toggle(rect, yoyoProperty.boolValue, yoyoProperty.displayName,"Button");
                }
                EditorGUILayout.EndHorizontal();
            }

            GUILayout.EndVertical();

            var tweens = serializedObject.FindProperty("tweens");
            if (tweenList == null)
            {
                tweenList = new EditableList<SequenceTweenProperty>(tweens.serializedObject, tweens, true, true, true, true);
            }

            tweenList.DoLayoutList();

            if (tweens.serializedObject.hasModifiedProperties)
            {
                tweens.serializedObject.ApplyModifiedProperties();

                UpdateCache();
                // TODO: instant preview is annoying with new tweens, maybe initialize them with current values?
//                InstantPreview();
            }

            if (childSequences.Count > 0 && !multiEdit)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Controls these sequences:");

                var transform = (target as MonoBehaviour).transform;

                childSequences.ForEach(tween =>
                {
                    var rect = EditorGUILayout.GetControlRect(false, 20);

                    rect.x += 20;
                    rect.width -= 40; 
                    
                    if (GUI.Button(rect, AnimationUtility.CalculateTransformPath(tween.transform, transform)))
                    {
                       Selection.activeObject = tween;
                    }
                });

                EditorGUILayout.Space();
            }

            if (!multiEdit)
            {
                EditorGUILayout.Space();

                if (!IsPlaying && GUILayout.Button("Play"))
                {
                    Stop();
                    Play();
                }
                if (IsPlaying && GUILayout.Button("Stop"))
                {
                    Stop();
                }
            }

            // draw event fields
            EditorGUILayout.BeginVertical(GUI.skin.box);
            EditorGUI.indentLevel++;
            var showEvents = serializedObject.FindProperty("showEvents");
            showEvents.boolValue = EditorGUILayout.Foldout(showEvents.boolValue, "Events");
            EditorGUI.indentLevel--;
            if (showEvents.boolValue)
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("onPlay"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("onComplete"));
            }

            GUILayout.EndVertical();
            serializedObject.ApplyModifiedProperties();

            // draw sequence id color coding
            var endY = EditorGUILayout.GetControlRect(false, 0).y;
            var stateMachine = rootPlayer?.SequenceStateMachine;
            var barWidth = stateMachine != null && stateMachine.StateName == sequenceId.stringValue ? 8 : 3;
            EditorGUI.DrawRect(new Rect(10 - barWidth / 2, startY, barWidth, endY - startY), TweenMachineEditorUtils.GetSequenceIdColor(sequenceId.stringValue));
        }

        private void DrawSequenceId(SerializedProperty property)
        {
            var rect = EditorGUILayout.GetControlRect(true);

            rect.width -= 16;
            EditorGUI.PropertyField(rect, property);

            var parentSequences = (property.serializedObject.targetObject as MonoBehaviour).transform.GetComponentsInParent<SequencePlayer>();
            var options = parentSequences.ToList().Select(player => player.SequenceId).Distinct().ToList();
            options.Sort();

            rect.x = rect.x + rect.width;
            rect.width = 16;
            var index = EditorGUI.Popup(rect, -1, options.ToArray());

            if (index != -1)
            {
                property.stringValue = options[index];
            }
        }

        private void DrawParentSequence(SerializedProperty sequenceId, SerializedProperty controllableByParent, bool multiEdit)
        {
            var parentSequence = TweenMachineUtils.FindParentSequence((target as MonoBehaviour).transform, sequenceId.stringValue);
            if (parentSequence != null && controllableByParent.boolValue && !multiEdit)
            {
                var stateMachine = rootPlayer?.SequenceStateMachine;
                
                var rect = EditorGUILayout.GetControlRect(false, stateMachine == null ? 32 : 52);
                EditorGUI.HelpBox(rect, $"Controlled by parent: {AnimationUtility.CalculateTransformPath(parentSequence.transform, null)}", MessageType.Info);

                var buttonWidth = 100f;
                rect.x = rect.width - buttonWidth;
                rect.y += 8f;
                rect.width = buttonWidth;
                rect.height = 16f;
                if (GUI.Button(rect, "Select"))
                {
                    Selection.activeObject = parentSequence;
                }

                if (stateMachine != null)
                {
                    rect.y += 20f;
                    if (GUI.Toggle(rect, stateMachine.StateName == sequenceId.stringValue, "Activate state", "Button"))
                    {
                        stateMachine.StateName = sequenceId.stringValue;
                    }
                }
            }
        }

        private void OnEnable()
        {
            UpdateCache();
        }

        private void OnDisable()
        {
            Stop();
        }

        private void UpdateCache()
        {
            childSequences = TweenMachineUtils.FindChildSequences((target as SequencePlayer).transform, serializedObject.FindProperty("sequenceId").stringValue);
            rootPlayer = TweenMachineUtils.FindRootSequence(target as SequencePlayer, serializedObject.FindProperty("sequenceId").stringValue);
        }

        private bool IsPlaying => (target as SequencePlayer).IsPlaying;

        private void Play()
        {
            if (activePreviewInstance)
            {
                activePreviewInstance.Stop();
            }

            (target as SequencePlayer).Play();
            activePreviewInstance = this;
        }

        private void Stop()
        {
            if (target == null) return;
            
            (target as SequencePlayer).Stop();
            if (activePreviewInstance == this)
            {
                activePreviewInstance = null;
            }
        }

        private void InstantPreview()
        {
            if (rootPlayer != null && rootPlayer.SequenceStateMachine != null && rootPlayer.SequenceStateMachine.StateName == rootPlayer.SequenceId)
            {
                rootPlayer.Play();
                rootPlayer.SeekToEnd();
            }
        }

        private void SetSequenceStateMachineToState()
        {
            if (rootPlayer != null && rootPlayer.SequenceStateMachine != null)
            {
                rootPlayer.SequenceStateMachine.StateName = rootPlayer.SequenceId;
            } 
        }
    }
}