using System;
using System.Collections.Generic;
using TweenMachine.Properties;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace TweenMachine.Editor
{
    [CustomEditor(typeof(SequenceStateMachine))]
    public class SequenceStateMachineEditor : UnityEditor.Editor
    {
        private ReorderableList reorderableList;
        
        public override void OnInspectorGUI()
        {
            var stateIndex = serializedObject.FindProperty("stateIndex");
            var states = serializedObject.FindProperty("states");

            if (reorderableList == null)
            {
                reorderableList = new ReorderableList(serializedObject, states, true, true, true, true)
                {
                    drawHeaderCallback = rect => { EditorGUI.LabelField(rect, name, EditorStyles.label); },

                    drawElementCallback = (rect, index, isActive, isFocused) =>
                    {
                        var element = states.GetArrayElementAtIndex(index);
                        rect.y += 1.0f;
                        rect.height -= 4.0f;

                        EditorGUI.BeginChangeCheck();
                        EditorGUI.PropertyField(rect, element, true);
                        if (EditorGUI.EndChangeCheck())
                        {
                            serializedObject.ApplyModifiedProperties();
                            
                            (target as SequenceStateMachine).UpdatePlayers();
                        }
                    },

                    onAddCallback = list =>
                    {
                        ++list.serializedProperty.arraySize;

                        serializedObject.ApplyModifiedProperties();
                        
                        var element = states.GetArrayElementAtIndex(list.serializedProperty.arraySize - 1);
                        var idProperty = element.FindPropertyRelative(nameof(SequenceState.id));
                        var playerProperty = element.FindPropertyRelative(nameof(SequenceState.sequencePlayer));

                        idProperty.stringValue = idProperty.stringValue != "" ? idProperty.stringValue + "-2" : idProperty.stringValue;  
                        playerProperty.objectReferenceValue = (target as SequenceStateMachine).CreateSequencePlayer(idProperty.stringValue);
                    },
                    
                    onRemoveCallback = list =>
                    {
                        var element = states.GetArrayElementAtIndex(list.index);
                        var player = element.FindPropertyRelative(nameof(SequenceState.sequencePlayer)).objectReferenceValue;

                        if (player != null)
                        {
                            DestroyImmediate(player);
                        }

                        states.DeleteArrayElementAtIndex(list.index);
                        
                        if (stateIndex.intValue == list.index)
                        {
                            stateIndex.intValue = 0;
                        }
                    }
                };
            }
            
            reorderableList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();
        }

        private void OnEnable()
        {
            (target as SequenceStateMachine).UpdatePlayers();
        }
    }
}