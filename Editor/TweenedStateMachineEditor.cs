/*using TweenMachine.Utils;
using UnityEditor;
using UnityEngine;

namespace TweenMachine.Editor
{
    [CustomEditor(typeof(TweenedStateMachine)), CanEditMultipleObjects]
    public class TweenedStateMachineEditor : UnityEditor.Editor
    {
        private EditorSequencePlayer sequencePlayer;
        
        public override void OnInspectorGUI()
        {
            var stateIndex = serializedObject.FindProperty("stateIndex");
            var sequences = serializedObject.FindProperty("sequences");
            
            EditorGUI.BeginChangeCheck();
            var stateIndexValue = EditorGUILayout.IntField("State Index", stateIndex.intValue);
            if (EditorGUI.EndChangeCheck())
            {
                (target as TweenedStateMachine).StateIndex = stateIndexValue;
            }
            
            sequences.arraySize = EditorGUILayout.IntField("Total States", sequences.arraySize);
            for (var i = 0; i < sequences.arraySize; i++)
            {
                EditorGUILayout.PropertyField(sequences.GetArrayElementAtIndex(i));
            }
            
            serializedObject.ApplyModifiedProperties();
        }
    }
}*/