using TweenMachine.Utils;
using UnityEditor;
using UnityEngine;

namespace TweenMachine.Properties.Editor
{
    [CustomPropertyDrawer(typeof(SequencePlayerReference))]
    public class SequencePlayerReferencePropertyDrawer : PropertyDrawer
    {
        private float controlHeight;
        private EditorSequencePlayer editorSequencePlayer;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            controlHeight = EditorGUIUtility.singleLineHeight;

            var sequencePlayerProperty = property.FindPropertyRelative("sequencePlayer");

            position.height = controlHeight;
            if (sequencePlayerProperty.objectReferenceValue != null)
            {
                position.width -= 100;
            }

            EditorGUI.PropertyField(position, sequencePlayerProperty);

            property.serializedObject.ApplyModifiedProperties();

            if (sequencePlayerProperty.objectReferenceValue != null)
            {
                position.x += position.width + 8;
                position.width = 92;
                if (GUI.Button(position, "Play"))
                {
                    var sequencePlayer = sequencePlayerProperty.objectReferenceValue as SequencePlayer;
                    editorSequencePlayer?.Clear();
                    editorSequencePlayer = new EditorSequencePlayer(sequencePlayer.transform, () => sequencePlayer.CreateSequence(1f));
                    editorSequencePlayer.Play();
                }
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return controlHeight;
        }
    }
}