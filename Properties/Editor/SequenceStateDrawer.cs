using TweenMachine.Utils;
using UnityEditor;
using UnityEngine;

namespace TweenMachine.Properties.Editor
{
    [CustomPropertyDrawer(typeof(SequenceState))]
    public class SequenceStateDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            var idProperty = property.FindPropertyRelative("id");

            var buttonWidth = 66f;

            var rect = new Rect(position);

            var colorRect = new Rect(position.x, position.y + 1f, 6f, position.height - 2f);
            EditorGUI.DrawRect(colorRect, TweenMachineEditorUtils.GetSequenceIdColor(idProperty.stringValue));

            rect.x += colorRect.width + 4f;
            rect.width -= buttonWidth + 4f + colorRect.width;
            EditorGUI.PropertyField(rect, idProperty, GUIContent.none);

            rect.width = buttonWidth;
            rect.x = position.x + position.width - rect.width;

            var stateMachine = (property.serializedObject.targetObject as SequenceStateMachine);

            var active = stateMachine.StateName == idProperty.stringValue;
            var newActive = GUI.Toggle(rect, active, "Active", "Button");
            if (newActive && !active)
            {
                stateMachine.StateName = idProperty.stringValue;
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }

            EditorGUI.EndProperty();
        }
    }
}