using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor;
using UnityEngine;

namespace TweenMachine.Properties.Editor
{
    [CustomPropertyDrawer(typeof(SequenceTweenEaseProperty))]
    public class SequenceTweenEasePropertyDrawer : PropertyDrawer
    {
        private static List<Ease> easeEnumValues = new List<Ease>
        {
            Ease.Linear,
            Ease.InSine,
            Ease.OutSine,
            Ease.InOutSine,
            Ease.InQuad,
            Ease.OutQuad,
            Ease.InOutQuad,
            Ease.InCubic,
            Ease.OutCubic,
            Ease.InOutCubic,
            Ease.InQuart,
            Ease.OutQuart,
            Ease.InOutQuart,
            Ease.InQuint,
            Ease.OutQuint,
            Ease.InOutQuint,
            Ease.InExpo,
            Ease.OutExpo,
            Ease.InOutExpo,
            Ease.InCirc,
            Ease.OutCirc,
            Ease.InOutCirc,
            Ease.InElastic,
            Ease.OutElastic,
            Ease.InOutElastic,
            Ease.InBack,
            Ease.OutBack,
            Ease.InOutBack,
            Ease.InBounce,
            Ease.OutBounce,
            Ease.InOutBounce,
            Ease.Flash,
            Ease.InFlash,
            Ease.OutFlash,
            Ease.InOutFlash
        };

        private static string[] easeOptions; 

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            InitEaseOptions();

            var easeProperty = property.FindPropertyRelative(nameof(SequenceTweenEaseProperty.ease));

            var easeValue = easeEnumValues.IndexOf((Ease) easeProperty.intValue);
            easeValue = easeValue >= 0 ? easeValue : easeEnumValues.IndexOf(SequenceTweenEaseProperty.DefaultEase); 
            var newValue = EditorGUI.Popup(position, easeValue, easeOptions);
            easeProperty.intValue = newValue >= 0 ? (int) easeEnumValues[newValue] : (int) SequenceTweenEaseProperty.DefaultEase;

            property.serializedObject.ApplyModifiedProperties();
        }

        private void InitEaseOptions()
        {
            if (easeOptions != null) return;

            easeOptions = new string[easeEnumValues.Count];
            var i = 0;
            foreach (var enumValue in easeEnumValues)
            {
                easeOptions[i] = Enum.GetName(typeof(Ease), enumValue);
                i++;
            }
        }
    }
}