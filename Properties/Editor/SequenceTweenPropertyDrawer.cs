using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using DG.Tweening;
using DG.Tweening.Core;
using TweenMachine.Utils;
using UnityEditor;
using UnityEngine;

namespace TweenMachine.Properties.Editor
{
    [CustomPropertyDrawer(typeof(SequenceTweenProperty))]
    public class SequenceTweenPropertyDrawer : PropertyDrawer
    {
        // TODO: Adjust for dark mode
        private Color warningYellow = new Color(1f, 0.91f, 0.65f);
        private GUIStyle recordButtonStyle;

        private static List<string> allComponentTypeNames;
        private Dictionary<Type, List<MethodInfo>> tweenMethodNames = new Dictionary<Type, List<MethodInfo>>();

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (recordButtonStyle == null)
            {
                recordButtonStyle = new GUIStyle(GUI.skin.button);
                recordButtonStyle.normal.textColor = Color.red;
            }

            if (allComponentTypeNames == null)
            {
                allComponentTypeNames = TweenReflectionUtils.GetDoTweenExtendedComponentTypes().Select(t => t.AssemblyQualifiedName).ToList();
                allComponentTypeNames.Sort();
            }

            var cachedIndentLevel = EditorGUI.indentLevel;

            var delay = property.FindPropertyRelative("delay");
            var targetObject = property.serializedObject.targetObject as MonoBehaviour;
            var componentTypeName = property.FindPropertyRelative("componentTypeName");
            var tweenMethodTypeName = property.FindPropertyRelative("tweenMethodTypeName");
            var tweenMethodName = property.FindPropertyRelative("tweenMethodName");
            var endValue = property.FindPropertyRelative("endValue");

            var totalWidth = position.width;
            var numbersWidth = Mathf.Clamp(totalWidth / 16f, 20f, 30f);
            var endValueWidth = Mathf.Clamp(totalWidth * 0.6f, 100f, 180f);
            var popupWidth = Mathf.Clamp((totalWidth - numbersWidth * 3 - endValueWidth) / 2f, 14f, 200f);

            var rect = position;

            // set warning background
            var type = Type.GetType(componentTypeName.stringValue);
            if (targetObject == null || type == null || targetObject.GetComponent(type) == null || string.IsNullOrEmpty(tweenMethodName.stringValue))
            {
                GUI.backgroundColor = warningYellow;
            }

            // draw delay
            rect.width = numbersWidth;
            rect.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.indentLevel = 0;
            EditorGUI.PropertyField(rect, delay, GUIContent.none);

            // draw tween target component picker
            AdvanceColumn(ref rect, popupWidth);
            EditorGUI.BeginChangeCheck();
            componentTypeName.stringValue = TweenMachineEditorUtils.ComponentPopup(rect, targetObject?.transform, componentTypeName.stringValue, allComponentTypeNames);

            var currentComponentType = Type.GetType(componentTypeName.stringValue);
            if (!tweenMethodNames.ContainsKey(currentComponentType))
            {
                tweenMethodNames[currentComponentType] = TweenReflectionUtils.GetDoTweenExtensionMethodNames(currentComponentType);
            }

            // draw tween method popup
            AdvanceColumn(ref rect, popupWidth + endValueWidth);
            DrawTweenMethodProperty(rect, currentComponentType, tweenMethodName, tweenMethodTypeName, endValue, popupWidth, endValueWidth, tweenMethodNames[currentComponentType], targetObject);

            // draw duration
            var duration = property.FindPropertyRelative("duration");
            AdvanceColumn(ref rect, numbersWidth);
            duration.floatValue = EditorGUI.FloatField(rect, duration.floatValue);

            // draw ease
            var ease = property.FindPropertyRelative(nameof(SequenceTweenProperty.ease));
            AdvanceColumn(ref rect, numbersWidth);
            EditorGUI.PropertyField(rect, ease);

            EditorGUI.indentLevel = cachedIndentLevel;
            GUI.backgroundColor = Color.white;
        }

        private static void AdvanceColumn(ref Rect position, float newWidth)
        {
            position.x += position.width + 2;
            position.width = newWidth - 2;

            EditorGUI.indentLevel = 0;
        }

        private void DrawTweenMethodProperty(Rect position, Type currentComponentType, SerializedProperty tweenMethodName, SerializedProperty tweenMethodTypeName, SerializedProperty endValue, float popupWidth, float endValueWidth, List<MethodInfo> tweenMethods, MonoBehaviour targetObject)
        {
            var tweenMethodDisplayNames = tweenMethods.Select(info => TweenMachineEditorUtils.GetMethodDisplayName(info)).ToArray();

            var rect = new Rect(position);
            rect.width = popupWidth;

            // TODO: remove legacy tweenMethod serialization 
            var selectedIndex = tweenMethods.FindIndex(info => info.ToString() == tweenMethodName.stringValue || info.Name.StartsWith(tweenMethodName.stringValue));
            selectedIndex = EditorGUI.Popup(rect, selectedIndex, tweenMethodDisplayNames);

            MethodInfo tweenMethod = null;

            if (selectedIndex == -1)
            {
                tweenMethodTypeName.stringValue = "";
                tweenMethodName.stringValue = "";
            } else
            {
                tweenMethod = tweenMethods[selectedIndex];
                tweenMethodTypeName.stringValue = tweenMethod.DeclaringType.AssemblyQualifiedName;
                tweenMethodName.stringValue = tweenMethod.ToString();
            }

            if (tweenMethod != null)
            {
                rect.x += popupWidth + 2;
                rect.width = 20;

                // capture current value
                if (GUI.Button(rect, "◉", recordButtonStyle))
                {
                    // create a dummy tween to to access it's getter 
                    var tween = TweenMachineUtils.CreateTween(
                        targetObject.transform,
                        currentComponentType.AssemblyQualifiedName,
                        tweenMethodTypeName.stringValue,
                        tweenMethodName.stringValue,
                        "",
                        10f,
                        0,
                        false,
                        Ease.Unset
                    );
                    // call getter to retrieve current value
                    var getter = tween.GetType().GetField("getter").GetValue(tween);
                    var currentValue = getter.GetType().GetMethod("Invoke").Invoke(getter, null);
                    endValue.stringValue = JsonUtility.ToJson(currentValue);
                    tween.Kill();
                }

                rect.x += 20 + 2;
                rect.width = endValueWidth - 26;
                endValue.stringValue = TweenMachineEditorUtils.JsonPropertyField(rect, tweenMethod.GetParameters()[1].ParameterType, endValue.stringValue);
            }
        }
    }
}