using System;
using UnityEngine;

namespace TweenMachine.Properties
{
    [Serializable]
    public class SequencePlayerReference
    {
        public SequencePlayer sequencePlayer;
    }
}