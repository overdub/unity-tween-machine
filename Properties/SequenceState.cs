using System;

namespace TweenMachine.Properties
{
    [Serializable]
    public class SequenceState
    {
        public string id;
        public SequencePlayer sequencePlayer;
    }
}