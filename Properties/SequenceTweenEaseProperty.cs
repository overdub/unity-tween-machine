using System;
using DG.Tweening;
using UnityEngine;

namespace TweenMachine.Properties
{
    public enum SequenceEaseType {Ease, AnimationCurve}
    
    [Serializable]
    public class SequenceTweenEaseProperty
    {
        public static readonly Ease DefaultEase = Ease.OutQuad; 
        
        public SequenceEaseType type = SequenceEaseType.Ease;
        public Ease ease = DefaultEase;
    }
}