using System;

namespace TweenMachine.Properties
{
    [Serializable]
    public class SequenceTweenProperty
    {
        public float delay;
        public float duration = 0.2f;

        public string componentTypeName;

        public string tweenMethodTypeName;
        public string tweenMethodName;
        public string endValue;

        public SequenceTweenEaseProperty ease;
    }
}