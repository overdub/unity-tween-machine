﻿using System.Collections.Generic;
using DataBinding.Lib;
using DG.Tweening;
using TweenMachine.Properties;
using TweenMachine.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace TweenMachine
{
    // TODO: add "skipFirstTime"?
    public class SequencePlayer : MonoBehaviour
    {
        [SerializeField] internal SequenceStateMachine sequenceStateMachine;
        public SequenceStateMachine SequenceStateMachine => sequenceStateMachine;

        [Tooltip("Used to control child sequences with the same id")]
        [SerializeField] private string sequenceId;

        public string SequenceId
        {
            get => sequenceId;
            set
            {
                if (!controllableByParent)
                {
                    var childSequences = TweenMachineUtils.FindChildSequences(transform, sequenceId);
                    childSequences.ForEach(sequence => sequence.SequenceId = value);
                }
                
                sequenceId = value;
                EditorUtils.SetDirty(this);
            }
        }

        public bool controllableByParent = true;

        public bool playOnEnable;
        
        [SerializeField] private bool showSequenceSettings;
        public bool loop;
        public int loopCount = -1;
        public bool yoyo;
        [SerializeField] internal List<SequenceTweenProperty> tweens = new List<SequenceTweenProperty>();
        
        [Tooltip("The sequence's delay in seconds")]
        public float delay;

        [Tooltip("The sequence's time scale factor")]
        public float timeScale = 1f;

        private EditorSequencePlayer player;

        [SerializeField] private bool showEvents;
        public UnityEvent onPlay = new UnityEvent();
        public UnityEvent onComplete = new UnityEvent();

        private void OnEnable()
        {
            if (playOnEnable)
            {
                Play();
            }
        }

        public Sequence CreateSequence(float parentTimeScale)
        {
            var localTimeScale = this.timeScale * parentTimeScale;
            var sequence = TweenMachineUtils.CreateSequence(transform, tweens, delay, localTimeScale, loop ? (loopCount < 0 ? int.MaxValue : loopCount) : 1, yoyo);

            var childSequences = TweenMachineUtils.FindChildSequences(transform, sequenceId);
            childSequences.ForEach(childSequencePlayer =>
            {
                sequence.Insert(delay * localTimeScale, childSequencePlayer.CreateSequence(localTimeScale));
            });

            sequence.onPlay += () => onPlay?.Invoke();
            sequence.onComplete += () =>
            {
                if (this == null) return;
                
                onComplete?.Invoke();
                EditorUtils.SetDirty(transform);
            };

            return sequence;
        }

        public bool IsPlaying => player?.IsPlaying ?? false;

        public void Play()
        {
            player?.Clear();
            player = new EditorSequencePlayer(transform, () => CreateSequence(1f));
            player.Play();
        }

        public void SeekToEnd(bool includeLoops = true)
        {
            player?.SeekToEnd(includeLoops);
        }
        
        public void Pause()
        {
            player?.Pause();
        }
        
        public void Stop()
        {
            player?.Stop();
        }

        public void Resume()
        {
            if (player == null)
            {
                Play();
                return;
            }

            player?.Play();
        }
    }
}