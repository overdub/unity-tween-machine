using System;
using System.Collections.Generic;
using System.Linq;
using TweenMachine.Properties;
using TweenMachine.Utils;
using UnityEngine;

namespace TweenMachine
{
    // TODO: implement layers
    [ExecuteAlways]
    public class SequenceStateMachine : MonoBehaviour
    {
        public event Action Changed;
        
        [SerializeField] private int stateIndex;

        public int StateIndex
        {
            get => stateIndex;
            set
            {
                value = Mathf.Clamp(value, 0, states.Count - 1);

                if (stateIndex == value) return;
                PauseState(stateIndex);

                stateIndex = value;

                PlayState(stateIndex);
                
                Changed?.Invoke();
            }
        }

        public string StateName
        {
            get => states[stateIndex].id;
            set => StateIndex = states.FindIndex(state => state.id == value);
        }

        [SerializeField] private List<SequenceState> states = new List<SequenceState>()
        {
            new SequenceState() {id = "Visible"},
            new SequenceState() {id = "Hidden"}
        };

//        private bool awakeIsOver;

        public string[] AvailableStates => states.Select(state => state.id).ToArray();

        public bool HasState(string id)
        {
            return states.FindIndex(state => state.id == id) >= 0;
        }

        public SequencePlayer GetPlayer(string id)
        {
            var state = states.Find(s => s.id == id);
            return state?.sequencePlayer;
        }

        public SequencePlayer CreateSequencePlayer(string stateName)
        {
            var existingPlayer = GetComponents<SequencePlayer>().ToList().Find(p => p.SequenceId == stateName);
            
            var player = existingPlayer ? existingPlayer : gameObject.AddComponent<SequencePlayer>();
            
            InitPlayer(player, stateName);
            
            return player;
        }

        private void PlayState(int index)
        {
            if (index < 0 || index >= states.Count) return;

            var state = states[index];
            
            UpdatePlayer(state);
            
            state.sequencePlayer.Play();
            
/*
            if (Application.isPlaying && !awakeIsOver)
            {
                state.sequencePlayer.SeekToEnd(false);
            }
*/
        }

        private void PauseState(int index)
        {
            if (index < 0 || index >= states.Count) return;

            var sequencePlayer = states[index].sequencePlayer;
            if (sequencePlayer != null)
            {
                sequencePlayer.Pause();
            }
        }

        private void InitPlayer(SequencePlayer player, string id)
        {
            player.sequenceStateMachine = this;
            player.controllableByParent = false;
            player.playOnEnable = false;
            player.SequenceId = id;
        }

        private void UpdatePlayer(SequenceState state)
        {
            if (state.sequencePlayer == null || state.sequencePlayer.transform != transform)
            {
                state.sequencePlayer = CreateSequencePlayer(state.id);
                return;
            }

            InitPlayer(state.sequencePlayer, state.id);
        } 
        
        public void UpdatePlayers()
        {
            states.ForEach(UpdatePlayer);
        }
        
        private void OnEnable()
        {
            UpdatePlayers();

            PlayState(stateIndex);
//            awakeIsOver = true;
        }
    }
}