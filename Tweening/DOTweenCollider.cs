using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace DG.Tweening
{
    public static class DOTweenCollider
    {
        public static TweenerCore<int, int, NoOptions> DOEnabled(this Collider target, bool endValue, float duration)
        {
            var t = DOTween.To(() => target.enabled ? 1 : 0, x => target.enabled = x == 1, endValue ? 1 : 0, duration);
            t.SetTarget(target);
            return t;
        }
        
        public static TweenerCore<int, int, NoOptions> DOEnabled(this BoxCollider target, bool endValue, float duration)
        {
            return target.DOEnabled(endValue, duration);
        }
        
        public static TweenerCore<int, int, NoOptions> DOEnabled(this SphereCollider target, bool endValue, float duration)
        {
            return target.DOEnabled(endValue, duration);
        }
        
        public static TweenerCore<int, int, NoOptions> DOEnabled(this CapsuleCollider target, bool endValue, float duration)
        {
            return target.DOEnabled(endValue, duration);
        }
    }
}