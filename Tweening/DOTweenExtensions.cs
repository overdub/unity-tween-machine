using System;
using System.Collections.Generic;

namespace DG.Tweening
{
    public class DOTweenExtensions
    {
        public static List<Type> extensions = new List<Type>()
        {
            typeof(DOTweenCollider),
            typeof(DOTweenMonoBehaviour),
            typeof(DOTweenParticleSystem),
            typeof(DOTweenTransform),
            typeof(DOTweenTweenMachine)
        };
    }
}