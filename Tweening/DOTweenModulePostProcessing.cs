/*
using System;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.Rendering;

namespace DG.Tweening
{
	public static class DOTweenModulePostProcessing
    {
        /// <summary>Tweens a Postprocessing Volume's weight.</summary>
        /// <param name="endValue">The end value to reach (0 to 1)</param><param name="duration">The duration of the tween</param>
        public static TweenerCore<float, float, FloatOptions> DOWeight(this Volume target, float endValue, float duration)
        {
            if (endValue < 0) endValue = 0;
            else if (endValue > 1) endValue = 1;
            
            TweenerCore<float, float, FloatOptions> t = DOTween.To(() => target.weight, x => target.weight = x, endValue, duration);
            t.SetTarget(target);
            return t;
        }
    }
}
*/
