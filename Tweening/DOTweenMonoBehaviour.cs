using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace DG.Tweening
{
    public static class DOTweenMonoBehaviour
    {
        public static TweenerCore<int, int, NoOptions> DOEnabled(this MonoBehaviour target, bool endValue, float duration)
        {
            var t = DOTween.To(() => target.enabled ? 1 : 0, x => target.enabled = x == 1, endValue ? 1 : 0, duration);
            t.SetTarget(target);
            return t;
        }
    }
}