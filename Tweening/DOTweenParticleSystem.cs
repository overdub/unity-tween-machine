using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace DG.Tweening
{
    public static class DOTweenParticleSystem
    {
        public static TweenerCore<int, int, NoOptions> DOEmitting(this ParticleSystem target, bool endValue, float duration)
        {
            var t = DOTween.To(() => target.emission.enabled ? 1 : 0, x =>
            {
                var emission = target.emission;
                emission.enabled = x == 1;
            }, endValue ? 1 : 0, duration);
            t.SetTarget(target);
            return t;
        }
    }
}