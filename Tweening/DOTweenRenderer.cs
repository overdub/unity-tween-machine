using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace DG.Tweening
{
    public static class DOTweenRenderer
    {
        public static TweenerCore<Color, Color, ColorOptions> DOSharedMaterialFade(this Renderer target, float endValue, float duration)
        {
            return target.sharedMaterial.DOFade(endValue, duration);
        }
        
        public static TweenerCore<Color, Color, ColorOptions> DOSharedMaterialColor(this Renderer target, Color endValue, float duration)
        {
            return target.sharedMaterial.DOColor(endValue, duration);
        }
        
        public static Tweener DOSharedMaterialBlendableColor(this Renderer target, Color endValue, float duration)
        {
            return target.sharedMaterial.DOBlendableColor(endValue, duration);
        }
    }
}