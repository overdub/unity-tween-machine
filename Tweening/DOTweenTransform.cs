using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace DG.Tweening
{
    public static class DOTweenTransform
    {
        public static TweenerCore<int, int, NoOptions> DOActive(this Transform target, bool endValue, float duration)
        {
            var t = DOTween.To(() => target.gameObject.activeSelf ? 1 : 0, x => target.gameObject.SetActive(x == 1), endValue ? 1 : 0, duration);
            t.SetTarget(target);
            return t;
        }
    }
}