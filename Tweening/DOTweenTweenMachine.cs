using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using TweenMachine;

namespace DG.Tweening
{
    public static class DOTweenTweenMachine
    {
        public static TweenerCore<int, int, NoOptions> DOState(this SequenceStateMachine target, string endValue, float duration)
        {
            var t = DOTween.To(() => target.StateName == endValue ? 1 : 0, x =>
            {
                if (x == 1) target.StateName = endValue;
            }, 1, duration);
            t.SetTarget(target);
            return t;
        }
    }
}