using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using DG.Tweening;
using TweenMachine.Editor;
using TweenMachine.Properties;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Object = UnityEngine.Object;

namespace TweenMachine.Utils
{
    public static class TweenMachineEditorUtils
    {
        public static string ComponentPopup(Rect rect, Transform root, string value, List<string> allComponentTypeNames)
        {
            List<string> componentTypeAssemblyNames = null;
            
            if (root != null)
            {
                var targetComponents = root.GetComponents<Component>();

                componentTypeAssemblyNames = targetComponents.ToList()
                    .Where(behaviour => behaviour != null)
                    .Select(behaviour => behaviour.GetType().AssemblyQualifiedName)
                    .Where(typeName =>
                    {
                        var baseTypes = TweenReflectionUtils.GetAllBaseTypesAssemblyQualifiedNames(typeName);
                        return allComponentTypeNames.Any(baseTypes.Contains);
                    }).ToList();
            } else
            {
                componentTypeAssemblyNames = new List<string>(allComponentTypeNames); 
            }

            var valueAdded = false;
            if (!string.IsNullOrEmpty(value) && !componentTypeAssemblyNames.Contains(value))
            {
                valueAdded = true;
                componentTypeAssemblyNames.Add(value);
            }
            
            componentTypeAssemblyNames.Sort();
            var selectedIndex = Mathf.Max(componentTypeAssemblyNames.IndexOf(value), 0);

            var typeDisplayNames = componentTypeAssemblyNames.Select(assemblyName =>
            {
                var ret = Type.GetType(assemblyName).FullName.Replace("UnityEngine.", "");
                if (valueAdded && assemblyName == value)
                {
                    ret = "-- " + ret;
                }
                return ret;
            }).ToArray();

            selectedIndex = EditorGUI.Popup(rect, selectedIndex, typeDisplayNames);

            return selectedIndex == -1 ? "" : componentTypeAssemblyNames[selectedIndex];
        }

        public static string GetMethodDisplayName(MethodInfo info)
        {
            var parameters = info.GetParameters();
            var name = info.Name.StartsWith("DO") ? info.Name.Substring(2) : info.Name;

            if (parameters.Length > 1)
            {
                return $"{name} ({info.GetParameters()[1].ParameterType.Name.Replace("Single", "float").Replace("Int32", "int")})";
            }
            
            return name;
        }
        
        public static string JsonPropertyField(Rect position, Type propertyType, string jsonValue)
        {
            if (propertyType == typeof(bool))
            {
                return EditorGUI.Toggle(position, DeserializeBoolValue(jsonValue, false)).ToString();
            }

            if (propertyType == typeof(float) || propertyType == typeof(double))
            {
                return EditorGUI.FloatField(position, DeserializeFloatValue(jsonValue, 0f)).ToString(CultureInfo.InvariantCulture);
            }

            if (propertyType == typeof(int))
            {
                return EditorGUI.IntField(position, DeserializeIntValue(jsonValue, 0)).ToString(CultureInfo.InvariantCulture);
            }

            if (propertyType == typeof(Vector2))
            {
                return JsonUtility.ToJson(EditorGUI.Vector2Field(position, "", DeserializeValue(jsonValue, Vector2.zero)));
            }

            if (propertyType == typeof(Vector3))
            {
                return JsonUtility.ToJson(EditorGUI.Vector3Field(position, "", DeserializeValue(jsonValue, Vector3.zero)));
            }

            if (propertyType == typeof(Color) || propertyType == typeof(Color32))
            {
                return JsonUtility.ToJson(EditorGUI.ColorField(position, DeserializeValue(jsonValue, Color.white)));
            }

            if (propertyType == typeof(string))
            {
                return EditorGUI.TextField(position, jsonValue);
            }

            return "";
        }

        private static bool DeserializeBoolValue(string serializedString, bool defaultValue)
        {
            try
            {
                return string.IsNullOrEmpty(serializedString) ? defaultValue : bool.Parse(serializedString);
            } catch (Exception)
            {
                return defaultValue;
            }
        }
        
        private static float DeserializeFloatValue(string serializedString, float defaultValue)
        {
            try
            {
                return string.IsNullOrEmpty(serializedString) ? defaultValue : float.Parse(serializedString, CultureInfo.InvariantCulture);
            } catch (Exception)
            {
                return defaultValue;
            }
        }
        
        private static int DeserializeIntValue(string serializedString, int defaultValue)
        {
            try
            {
                return string.IsNullOrEmpty(serializedString) ? defaultValue : int.Parse(serializedString, CultureInfo.InvariantCulture);
            } catch (Exception)
            {
                return defaultValue;
            }
        }
        
        private static T DeserializeValue<T>(string serializedString, T defaultValue)
        {
            try
            {
                return string.IsNullOrEmpty(serializedString) ? defaultValue : JsonUtility.FromJson<T>(serializedString);
            } catch (Exception)
            {
                return defaultValue;
            }
        }        
        
        public static Color GetSequenceIdColor(string sequenceId)
        {
            var bytes = Encoding.UTF8.GetBytes(sequenceId);

            var hueShift = 0f;
            foreach (var b in bytes)
            {
                hueShift += b / 255f * 4f;
                hueShift = hueShift - Mathf.Floor(hueShift);
            }
            
            return Color.HSVToRGB(hueShift, 0.6f, 0.9f);
        }
    }
}