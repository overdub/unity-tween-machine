using System;
using DG.Tweening;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace TweenMachine.Utils
{
    public class EditorSequencePlayer
    {
        private bool playing;

        private Transform root;
        private Func<Tween> sequenceFactory;
        private Tween dgSequence;

        public EditorSequencePlayer(Transform root, Func<Tween> sequenceFactory)
        {
            this.root = root;
            this.sequenceFactory = sequenceFactory;
        }

        public float ElapsedPercentage => dgSequence?.ElapsedPercentage(false) ?? 0f;
        public float Elapsed => dgSequence?.Elapsed(false) ?? 0f;
        public float Duration => dgSequence?.Duration(false) ?? 0f;

        public bool HasSequence => dgSequence != null;
        public bool IsPlaying => playing;

        public void Rewind()
        {
            Seek(0);
        }

        public void Clear()
        {
            Pause();

            if (dgSequence == null) return;

            if (dgSequence.active)
            {
                dgSequence?.Kill();
            }

            dgSequence = null;
        }

        public void Play()
        {
            if (playing) return;

            if (dgSequence == null)
            {
                dgSequence = CreateSequence();
                dgSequence.Play();
            }

            playing = true;

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                dgSequence.onPlay?.Invoke();

                Seek(0);
                EditorApplication.update += EditorUpdate;
                EditorApplication.playModeStateChanged += EditorPlayModeStateChanged;
            }
#endif
        }

        private Tween CreateSequence()
        {
            var sequence = sequenceFactory();

            sequence.Pause();
            sequence.ForceInit();

            return sequence;
        }

        public void Pause()
        {
            if (!playing) return;
            playing = false;

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                EditorApplication.update -= EditorUpdate;
            }
#endif
            dgSequence?.Pause();
        }

        public void Stop()
        {
            Pause();

            if (dgSequence != null && dgSequence.active)
            {
                dgSequence?.Kill();
            }

            dgSequence = null;
        }

        public void Seek(float position)
        {
            if (dgSequence == null) return;

#if UNITY_EDITOR
            if (root == null)
            {
                Clear();
                return;
            }

            dgSequence.fullPosition = position;

            EditorUtility.SetDirty(root);
#else
            dgSequence.fullPosition = position;
#endif
        }

        public void SeekToEnd(bool includeLoops = true)
        {
            Seek(dgSequence.Duration(includeLoops));
        }

#if UNITY_EDITOR
        private void EditorUpdate()
        {
            if (!playing || dgSequence == null) return;

//            Seek(dgSequence.fullPosition + Mathf.Max(Time.deltaTime, 1f / 30f));
            Seek(dgSequence.fullPosition + Time.deltaTime * dgSequence.timeScale);

            if (dgSequence != null && dgSequence.fullPosition >= dgSequence.Duration())
            {
                dgSequence.onComplete?.Invoke();
                Seek(dgSequence.Duration());
                Pause();
            }
        }

        private void EditorPlayModeStateChanged(PlayModeStateChange playModeStateChange)
        {
            if (playModeStateChange == PlayModeStateChange.EnteredEditMode
                || playModeStateChange == PlayModeStateChange.EnteredPlayMode) return;

            Stop();
            DOTween.KillAll();
        }

#endif
    }
}