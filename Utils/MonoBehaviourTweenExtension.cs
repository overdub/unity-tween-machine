using DG.Tweening;
using UnityEngine;

namespace TweenMachine.Utils
{
    public static class MonoBehaviourTweenExtension
    {
        public static Tween PlayTween(this MonoBehaviour behaviour, Tween tween)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                var player = new EditorSequencePlayer(behaviour.transform, () => tween);
                player.Play();
            }
#endif
            return tween;
        }
    }
}