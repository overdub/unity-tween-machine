using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using DG.Tweening;
using TweenMachine.Properties;
using UnityEngine;

namespace TweenMachine.Utils
{
    public static class TweenMachineUtils
    {
        private static Dictionary<string, MethodInfo> methodCache = new Dictionary<string, MethodInfo>();
        
        public static Sequence CreateSequence(Transform root, List<SequenceTweenProperty> tweens, float delay, float timeScale, int loop, bool loopYoyo)
        {
            var sequence = DOTween.Sequence();

            tweens.ForEach(tween => { sequence.Insert((delay + tween.delay) * timeScale, CreateTween(root, tween, timeScale, loop, loopYoyo)); });

            return sequence;
        }

        public static Tween CreateTween(Transform root, SequenceTweenProperty tween, float timeScale, int loop, bool loopYoyo)
        {
            return CreateTween(root, tween.componentTypeName, tween.tweenMethodTypeName, tween.tweenMethodName, tween.endValue, tween.duration * timeScale, loop, loopYoyo, tween.ease.ease);
        }

        public static Tween CreateTween(Component tweenTarget, string tweenMethodTypeName, string tweenMethodName, string endValue, float duration, int loop, bool loopYoyo, Ease ease)
        {
            if (tweenTarget == null)
            {
                Debug.LogWarning("Couldn't find tween target");
                return null;
            }

            var componentType = tweenTarget.GetType();
            var component = tweenTarget.GetComponent(componentType);
            if (component == null)
            {
                Debug.LogWarning("Couldn't find tween target component");
                return null;
            }

            var tweenMethodType = Type.GetType(tweenMethodTypeName);
            if (tweenMethodType == null)
            {
                Debug.LogWarning("Couldn't find tween method");
                return null;
            }

            var method = GetCachedMethodInfo(tweenMethodType, tweenMethodName, componentType);

            if (method == null)
            {
                Debug.LogWarning("Couldn't find tween method");
                return null;
            }

            var parameters = method.GetParameters();
            var arguments = new List<object>()
            {
                component, GetTweenMethodEndValue(method, endValue), duration
            };

            if (parameters.Length > 3)
            {
                for (var i = 3; i < parameters.Length; i++)
                {
                    arguments.Add(parameters[i].DefaultValue);
                }
            }

            var tween = method.Invoke(component, arguments.ToArray()) as Tween;
            tween.SetLoops(loop, loopYoyo ? LoopType.Yoyo : LoopType.Restart);
            tween.SetEase(ease);
            return tween;
        }

        private static MethodInfo GetCachedMethodInfo(Type tweenMethodType, string tweenMethodName, Type componentType)
        {
            if (methodCache.ContainsKey(tweenMethodName))
            {
                return methodCache[tweenMethodName];
            }

            var types = TweenReflectionUtils.GetAllBaseTypes(componentType);
            var method = tweenMethodType.GetMethods(BindingFlags.Public | BindingFlags.Static).ToList().Find(info =>
            {
                var methodParameters = info.GetParameters();
                return (info.ToString() == tweenMethodName || info.Name == tweenMethodName) && types.Contains(methodParameters[0].ParameterType);
            });

            methodCache[method.ToString()] = method;
            
            return method;
        }

        public static Tween CreateTween(Transform root, string componentTypeName, string tweenMethodTypeName, string tweenMethodName, string endValue, float duration, int loop, bool loopYoyo, Ease ease)
        {
            if (root == null)
            {
                Debug.LogWarning("Tween query root is null");
                return null;
            }

            var componentType = Type.GetType(componentTypeName);
            var component = root.GetComponent(componentType);
            if (component == null)
            {
                Debug.LogWarning("Couldn't find tween target component");
                return null;
            }

            return CreateTween(component, tweenMethodTypeName, tweenMethodName, endValue, duration, loop, loopYoyo, ease);
        }

        public static object GetTweenMethodEndValue(MethodInfo method, string endValue)
        {
            var parameters = method.GetParameters();
            var propertyType = parameters[1].ParameterType;
            if (propertyType == typeof(bool))
            {
                return !string.IsNullOrEmpty(endValue) && bool.Parse(endValue);
            }

            if (propertyType == typeof(float) || propertyType == typeof(double))
            {
                return string.IsNullOrEmpty(endValue) ? 0f : float.Parse(endValue, CultureInfo.InvariantCulture);
            }

            if (propertyType == typeof(int))
            {
                return string.IsNullOrEmpty(endValue) ? 0 : int.Parse(endValue, CultureInfo.InvariantCulture);
            }

            if (propertyType == typeof(Vector2))
            {
                return string.IsNullOrEmpty(endValue) ? Vector2.zero : JsonUtility.FromJson<Vector2>(endValue);
            }

            if (propertyType == typeof(Vector3))
            {
                // workaround for serialization bug => serialize method names with signature in future!
                var floatValue = 0f;
                if (float.TryParse(endValue, out floatValue))
                {
                    return new Vector3(floatValue, floatValue, floatValue);
                }
                return string.IsNullOrEmpty(endValue) ? Vector3.zero : JsonUtility.FromJson<Vector3>(endValue);
            }

            if (propertyType == typeof(Color))
            {
                return string.IsNullOrEmpty(endValue) ? Color.white : JsonUtility.FromJson<Color>(endValue);
            }

            if (propertyType == typeof(string))
            {
                return endValue;
            }

            return null;
        }

        public static SequencePlayer FindParentSequence(Transform transform, string sequenceId)
        {
            if (string.IsNullOrEmpty(sequenceId) || transform == null || transform.parent == null) return null;

            var sequencePlayers = transform.parent.GetComponents<SequencePlayer>();

            foreach (var sequencePlayer in sequencePlayers)
            {
                if (sequencePlayer != null && sequencePlayer.SequenceId == sequenceId)
                {
                    return sequencePlayer;
                }
            }

            return FindParentSequence(transform.parent, sequenceId);
        }
        
        public static SequencePlayer FindRootSequence(SequencePlayer player, string sequenceId)
        {
            SequencePlayer parentSequence = null;
            var nextParent = player;
            while ((nextParent = FindParentSequence(nextParent.transform, sequenceId)) != null)
            {
                parentSequence = nextParent;
            }

            if (parentSequence == null)
            {
                return player;
            }
            
            return parentSequence;
        }
        
        public static void FindChildSequences(Transform root, string sequenceId, ref List<SequencePlayer> childSequences)
        {
            if (string.IsNullOrEmpty(sequenceId)) return;
            
            for (var i = 0; i < root.childCount; i++)
            {
                var child = root.GetChild(i);

                var foundChild = false;
                
                var sequencePlayers = child.GetComponents<SequencePlayer>();
                foreach (var sequencePlayer in sequencePlayers)
                {
                    if (sequencePlayer != null && sequencePlayer.SequenceId == sequenceId)
                    {
                        if (sequencePlayer.controllableByParent)
                        {
                            childSequences.Add(sequencePlayer);
                        }
                        foundChild = true;
                    }
                }

                if (!foundChild)
                {
                    FindChildSequences(child, sequenceId, ref childSequences);
                }
            }
        }

        public static List<SequencePlayer> FindChildSequences(Transform root, string sequenceId)
        {
            var childSequences = new List<SequencePlayer>();

            FindChildSequences(root, sequenceId, ref childSequences);

            return childSequences;
        }
    }
}