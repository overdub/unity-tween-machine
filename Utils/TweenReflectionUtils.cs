using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using DataBinding;
using DG.Tweening;

namespace TweenMachine.Utils
{
    public static class TweenReflectionUtils
    {
        public static IEnumerable<MethodInfo> GetAllTweeningExtensionMethods(Assembly extensionsAssembly)
        {
            var query = from t in extensionsAssembly.GetTypes()
                where !t.IsGenericType /*&& t.Namespace == "DG.Tweening"*/
                from m in t.GetMethods(BindingFlags.Public | BindingFlags.Static)
                where m.Name.StartsWith("DO") && m.IsDefined(typeof(ExtensionAttribute), false) && !m.IsGenericMethod
                select m;

            return query;
        }
        
        public static List<Type> GetAllBaseTypes(Type type)
        {
            var types = new List<Type>();
            var t = type;
            while (t != null)
            {
                types.Add(t);
                t = t.BaseType;
            }

            return types;
        }
        
        public static List<string> GetAllBaseTypesAssemblyQualifiedNames(string typeName)
        {
            var types = new List<string>();
            var t = Type.GetType(typeName);
            while (t != null)
            {
                types.Add(t.AssemblyQualifiedName);
                t = t.BaseType;
            }

            return types;
        }
        
        public static IEnumerable<MethodInfo> GetTweeningExtensionMethods(Type type, Assembly extensionsAssembly)
        {
            var types = GetAllBaseTypes(type);
            
            var query = from t in extensionsAssembly.GetTypes()
                where !t.IsGenericType /*&& t.Namespace == "DG.Tweening"*/
                from m in t.GetMethods(BindingFlags.Public | BindingFlags.Static)
                where m.IsDefined(typeof(ExtensionAttribute), false)
                where types.Contains(m.GetParameters()[0].ParameterType)
                select m;

            return query;
        }

        public static List<MethodInfo> GetDoTweenExtensionMethodNames(Type currentComponentType)
        {
            var assembly = Assembly.GetAssembly(typeof(DOTweenModuleUI));

            var tweenMethods = GetTweeningExtensionMethods(currentComponentType, assembly).ToList();
            tweenMethods.AddRange(GetTweeningExtensionMethods(currentComponentType, Assembly.GetAssembly(typeof(DOTween))));
            tweenMethods.AddRange(GetTweeningExtensionMethods(currentComponentType, Assembly.GetAssembly(typeof(TweenReflectionUtils))));
            tweenMethods.Sort((a, b) => string.Compare(a.Name, b.Name, StringComparison.InvariantCulture));

            return tweenMethods;
        }
        
        public static List<Type> GetDoTweenExtendedComponentTypes()
        {
            var assembly = Assembly.GetAssembly(typeof(DOTweenModuleUI));

            var tweenMethods = GetAllTweeningExtensionMethods(assembly).ToList();
            tweenMethods.AddRange(GetAllTweeningExtensionMethods(Assembly.GetAssembly(typeof(DOTween))));
            
            var types = tweenMethods.Select(info => info.GetParameters()[0].ParameterType).Distinct().ToList();
            types.Sort((a, b) => string.Compare(a.Name, b.Name, StringComparison.InvariantCulture));

            return types;
        }

        public static T GetLinkedAttributeValue<T>(object context, string name, T defaultValue) 
        {
            var field = context.GetType().GetField(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly);
            if (field != null && field.FieldType == typeof(T))
            {
                return (T) field.GetValue(context);
            }
            
            var property = context.GetType().GetProperty(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly);
            if (property != null && property.PropertyType == typeof(T))
            {
                return (T) property.GetValue(context);
            }
            
            var method = context.GetType().GetMethod(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.DeclaredOnly);
            if (method != null && method.ReturnType == typeof(T) && method.GetParameters().Length == 0)
            {
                return (T) method.Invoke(context, null);
            }

            return defaultValue;
        }
    }
}